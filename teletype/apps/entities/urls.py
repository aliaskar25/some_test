from django.urls import path

from rest_framework.routers import DefaultRouter

from apps.entities.views import EntityViewSet


urlpatterns = [
    path('', EntityViewSet.as_view())
]
