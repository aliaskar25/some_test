from rest_framework import serializers


class EntitySerializer(serializers.Serializer):
    value = serializers.IntegerField(required=False)
    properties = serializers.SerializerMethodField(read_only=True)

    def get_properties(self, obj):
        return {i.key: i.value for i in obj.properties.all()}
