
from rest_framework.views import APIView
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.entities.models import Entity, Property
from apps.entities.serializers import EntitySerializer


class EntityViewSet(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated, )

    def get(self, request, *args, **kwargs):
        entities = Entity.objects.prefetch_related('properties').all()
        serializer = EntitySerializer(entities, many=True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        for property_values, entity_value in request.data.items():

            if not isinstance(entity_value, int):
                return Response('field data[value] must be integer', status=400)

            key = property_values.split('[')[0]
            value = property_values.split('[')[1].replace(']', '')

            _property = Property.objects.create(
                key=key,
                value=value
            )

            entity, _ = Entity.objects.get_or_create(
                modified_by=request.user, value=entity_value
            )
            entity.properties.add(_property)

        return Response(request.data)
